package com.inventory.zeph;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.inventory.zeph.frames.CustomerUI;
import com.inventory.zeph.frames.ProductUI;
import com.inventory.zeph.frames.SupplierUI;
import com.inventory.zeph.utility.UiUtility;


public class BootClass {
	
	public BootClass() {

		JTextArea textArea1=UiUtility.getJTextArea("Enter the Text..");
		textArea1.setBackground(new Color(255,100,0));
		textArea1.setSize(new Dimension(5,5));
		
		JLabel topLabel=UiUtility.getJLabel("Welcome to Zeph Inventory");
		JLabel bottomLabel=UiUtility.getJLabel("@Copy Right reserved to Asif\n");
		bottomLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		//JButton prodButton=UiUtility.getJButton("Product");
		JButton prodButton = new JButton("Product");
		prodButton.setLocation(100, 50);
		//prodButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Data/icons8-supplier-16.png")));
		prodButton.addActionListener(e->{
			ProductUI.loaddProductPage();
		});

		JButton suppButton=UiUtility.getJButton("Supplier");
		suppButton.setLocation(100, 120);
		suppButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Data/icons8-supplier-16.png")));
		suppButton.addActionListener(e->{
			SupplierUI.loaddSupplierPage();
		});
		
		JButton custButton=UiUtility.getJButton("Customer");
		custButton.setLocation(100, 190);
		custButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Data/icons8-used-product-13.png")));
		custButton.addActionListener(e->{
			CustomerUI.loaddCustomerPage();
		});
		
		Icon icon = new ImageIcon(getClass().getResource("/Data/icons8-used-product-40.png"));
		JLabel iconLabel_prod = new JLabel(icon);
		iconLabel_prod.setSize(100, 100);
		iconLabel_prod.setLocation(0, 35);
		
		JPanel buttonPanel=UiUtility.getJPanel();
		buttonPanel.setLayout(null);
		buttonPanel.add(prodButton);
		buttonPanel.add(suppButton);
		buttonPanel.add(custButton);
		buttonPanel.add(iconLabel_prod);
		
		
		JPanel mainPanel=UiUtility.getJPanel();
		mainPanel.add(topLabel,BorderLayout.NORTH);
		mainPanel.add(buttonPanel);
		mainPanel.add(bottomLabel,BorderLayout.PAGE_END);
		
		
		
		JFrame frame=UiUtility.getJFrame("Main Frame");
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.setSize(new Dimension(500, 450));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.pack();
		frame.setVisible(true);
		
		
		JFrame frame1=UiUtility.getFrame1();
		//frame1.setVisible(true);

		
		
	}
	
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
		BootClass.class.getResource("/Data/money-icon.png");
        SwingUtilities.invokeLater(() -> {
            new BootClass();
        });
	}
	

}
