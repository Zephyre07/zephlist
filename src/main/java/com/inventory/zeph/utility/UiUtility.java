package com.inventory.zeph.utility;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class UiUtility {

	public static JFrame  getFrame1() {
		

	      JTextArea englishTextArea = new JTextArea(20,20);
	      englishTextArea.setText("Hello World");
	      englishTextArea.setLineWrap(true);
	      englishTextArea.setWrapStyleWord(true);
	      englishTextArea.setMargin(new Insets(5, 5, 5,5));
	      JLabel englishTextLabel = new JLabel("English Text");
	      englishTextLabel.setHorizontalAlignment(SwingConstants.CENTER);

	      JButton clearEnglishText = new JButton("<< Clear Text");
	      JButton englishToMorseBt = new JButton("English >> Morse");

			
			  JPanel englishControlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			  englishControlPanel.add(clearEnglishText);
			  englishControlPanel.add(englishToMorseBt);
			 System.out.println("INSIDE------------");

	      JPanel englishTextPanel = new JPanel();
	      englishTextPanel.setLayout(new BorderLayout());
	      englishTextPanel.add(englishTextLabel, BorderLayout.NORTH);
	      englishTextPanel.add(new JScrollPane(englishTextArea), BorderLayout.CENTER);
	     // englishTextPanel.add(englishControlPanel, BorderLayout.SOUTH);

	      JTextArea morseTextArea = new JTextArea();
	      morseTextArea.setText(".-");
	      morseTextArea.setLineWrap(true);
	      morseTextArea.setWrapStyleWord(true);
	      morseTextArea.setLineWrap(true);
	      morseTextArea.setMargin(new Insets(5, 5, 5,5));
	      morseTextArea.setFont(new Font("", 0, 20));

	      JLabel morseTextLabel = new JLabel("Morse Code");
	      morseTextLabel.setHorizontalAlignment(SwingConstants.CENTER);


	      JButton morseToEnglishBt = new JButton("Morse >> English");
	      JButton clearMorseText = new JButton("Clear Text >>");

	      JPanel morseControlPanel = new JPanel();
	      morseControlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
	      morseControlPanel.add(morseToEnglishBt);
	      morseControlPanel.add(clearMorseText);

	      JPanel morseTextPanel = new JPanel();
	      morseTextPanel.setLayout(new BorderLayout());
	      morseTextPanel.add(morseTextLabel, BorderLayout.NORTH);
	      morseTextPanel.add(new JScrollPane(morseTextArea), BorderLayout.CENTER);
	      morseTextPanel.add(morseControlPanel, BorderLayout.SOUTH);


		
		JFrame frame = new JFrame();
	      frame.setTitle("Morse Translator by Seun Matt (@SeunMatt2)");
	      frame.setLayout(new BorderLayout());
	      frame.add(morseTextPanel, BorderLayout.CENTER);
	      frame.setSize(new Dimension(800, 650));
	      frame.setLocationRelativeTo(null);
	      frame.setResizable(true);
	     // frame.setVisible(true);
	      
	      
	      return frame;
		
	}
	
	public static JFrame getJFrame(String title) {
		
		JFrame frame=new JFrame();
		frame.setTitle(title);
		frame.setLayout(new BorderLayout());
		frame.setSize(new Dimension(800, 650));
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		return frame;
		
		
		
	}
	
	public static JPanel getJPanel() {

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		return panel;

	}

	public static JButton getJButton(String title) {

		JButton btn=new JButton(title);
		btn.setSize(100, 50);
		return btn;
	}
	
	public static JTextArea getJTextArea(String text) {

		 JTextArea textArea = new JTextArea();
		 textArea.setText(text);
		 textArea.setLineWrap(true);
		 textArea.setWrapStyleWord(true);
		 textArea.setLineWrap(true);
		 textArea.setMargin(new Insets(5, 5, 5,5));
		 textArea.setFont(new Font("", 0, 10));
		 
		 return textArea;

	}
	
	public static JLabel getJLabel(String text) {

		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.CENTER);

		return label;
	}
	
	
}
