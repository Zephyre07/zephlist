package com.inventory.zeph.frames;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.inventory.zeph.utility.UiUtility;

public class ProductUI {
	
	public static void loaddProductPage() {
		  
		String data[][]={ {"101","Amit","670000"},    
                  {"102","Jai","780000"},    
                  {"101","Sachin","700000"}};    
String column[]={"ID","NAME","SALARY"};         
JTable jt=new JTable(data,column);  
jt.setEnabled(false);
jt.setBounds(30,40,200,300);         
JScrollPane sp=new JScrollPane(jt);    
		
		JPanel panel=UiUtility.getJPanel();
		panel.add(sp);
		
		
		JFrame frame1=UiUtility.getJFrame("PRODUCT");
		frame1.add(panel);
		frame1.setVisible(true);
	}

}
